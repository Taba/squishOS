#!/bin/sh
set -ex

## Make sure we are compiling from musl
##
libc=$(ldd /bin/ls | grep 'musl' | head -1 | cut -d ' ' -f1)
if [ -z $libc ]; then
    # This is not Musl.
    echo "this is not musl"
    exit 1
fi
echo "this is musl"

## Install Wowlet build Depends
##
sudo xbps-install -SuAy gnutls-devel qrencode-devel \
libX11-devel cairo-devel qt5-declarative-devel \
qt5-quickcontrols2-devel qt5-svg-devel \
qt5-multimedia-devel qt5-websockets-devel \
czmq-devel libsodium-devel libXinerama-devel \
libXfixes-devel libzbar-devel libgcrypt-devel \
libzip-devel cmake base-devel boost-devel

## Get and compile/install wownero-seed
##
git clone https://git.wownero.com/wowlet/wownero-seed external/wownero-seed ||:
cd external/wownero-seed/src
cmake ../.
sudo make install
cd ../../..

## Get and compile wowlet + wownero cli
##
git clone https://git.wownero.com/wowlet/wowlet external/wowlet ||:
cd external/wowlet
git submodule update --init --recursive
patch -Np1 wownero/contrib/epee/include/storages/parserse_base_utils.h <<'EOF' ||:
--- a/contrib/epee/include/storages/parserse_base_utils.h
+++ b/contrib/epee/include/storages/parserse_base_utils.h
@@ -30,6 +30,7 @@
 
 #include <boost/utility/string_ref_fwd.hpp>
 #include <string>
+#include <cstdint>
 
 namespace epee 
 {
EOF
patch -Np1 wownero/contrib/epee/include/file_io_utils.h <<'EOF' ||:
--- a/contrib/epee/include/file_io_utils.h
+++ b/contrib/epee/include/file_io_utils.h
@@ -29,6 +29,7 @@
 
 #include <string>
 #include <ctime>
+#include <cstdint>
 
 namespace epee
 {
EOF
patch -Np1 wownero/src/common/combinator.h <<'EOF' ||:
--- a/src/common/combinator.h
+++ b/src/common/combinator.h
@@ -34,6 +34,7 @@
 #include <iostream>
 #include <vector>
 #include <stdexcept>
+#include <cstdint>
 
 namespace tools {
 
EOF
patch -Np1 wownero/src/wallet/api/wallet2_api.h <<'EOF' ||:
--- a/src/wallet/api/wallet2_api.h
+++ b/src/wallet/api/wallet2_api.h
@@ -38,6 +38,7 @@
 #include <ctime>
 #include <iostream>
 #include <stdexcept>
+#include <cstdint>
 
 //  Public interface for libwallet library
 namespace Monero {
EOF
cmake -DMANUAL_SUBMODULES=1 -DUSE_DEVICE_TREZOR=OFF -DUSE_SINGLE_BUILDDIR=ON -DDEV_MODE=ON -DSTACK_TRACE=OFF -DARCH=x86-64
make -j"$(nproc)"
install -m755 bin/* ../../include/usr/bin
install -Dm644 src/assets/images/appicons/256x256.png ../../include/usr/share/icons/wowlet.png
install -Dm644 src/assets/org.wowlet.wowlet.desktop ../../include/etc/skel/.local/share/applications/wowlet.desktop

## Uninstall WOWlet build deps
##
sudo xbps-remove -oy
