## Make sure we are compiling from musl
##
libc=$(ldd /bin/ls | grep 'musl' | head -1 | cut -d ' ' -f1)
if [ -z $libc ]; then
    # This is not Musl.
    echo "this is not musl"
    exit 1
fi
echo "this is musl"

## Install Wowlet build Depends
##
sudo xbps-install -SuAy base-devel cmake boost-devel \
openssl-devel unbound-devel libsodium-devel zlib-devel \
qt6-base-devel qt6-svg-devel qt6-websockets-devel \
qt6-multimedia-devel libgcrypt-devel libzip-devel \
hidapi-devel protobuf protobuf-devel qrencode-devel \
zxing-cpp-devel

## Get and compile feather + monero cli
##
git clone https://github.com/feather-wallet/feather external/feather
cd external/feather
git submodule update --init --recursive
cmake -DMANUAL_SUBMODULES=1 -DUSE_DEVICE_TREZOR=OFF -DUSE_SINGLE_BUILDDIR=ON -DDEV_MODE=ON -DSTACK_TRACE=OFF
make
cp bin/* ../../include/usr/bin
cp src/assets/images/appicons/256x256.png ../../include/usr/share/icons/feather.png
cp src/assets/feather.desktop ../../include/etc/skel/.local/share/applications/feather.desktop
cd ../..
